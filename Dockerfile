FROM ubuntu:xenial

RUN apt-get update && apt-get install -y --no-install-recommends \
	g++ \
	libjemalloc-dev \
	libpcap-dev \
	python \
	libpcre2-8-0 \
	autoconf \
	zlib1g-dev \
	flex \
	byacc \
	cmake \
	libtool \
	libtool-bin \
	subversion \
	rpm \
	libreadline6 libreadline6-dev \
	libnuma-dev \
	git \
	subversion \
	curl \
	zip \
	unzip \
	wget \
	build-essential \
	linux-headers-`uname -r` \
	ca-certificates \
    && update-ca-certificates \
    && rm -rf /var/lib/apt/lists/*

RUN wget http://www.tcpdump.org/release/libpcap-1.5.3.tar.gz && \
	tar xzvf ./libpcap-1.5.3.tar.gz && \
	cd ./libpcap-1.5.3/ && \
	./configure && \
	make && \
	make install

RUN wget http://therouter.net/downloads/proplib-0.6.3.tar.xz && \
	tar xvf ./proplib-0.6.3.tar.xz && \
	cd ./proplib-0.6.3 && \
	./configure && \
	make && \
	make install

RUN wget http://therouter.net/downloads/libcdb.tar.gz && \
	tar xvf ./libcdb.tar.gz && \
	cd ./libcdb && \
	wget http://therouter.net/downloads/libcdb_alexk.patch && \
	cat libcdb_alexk.patch | patch -p1 && \
	export LIBDIR=lib && \
	export INCDIR=include && \
	export DESTDIR=/usr/local && \
	make all && \
	make install; exit 0

RUN wget http://therouter.net/downloads/libqsbr.tar.gz && \
	tar xzvf libqsbr.tar.gz && \
	cd ./libqsbr/src && \
	wget http://therouter.net/downloads/libqsbr_alexk.patch && \
	cat libqsbr_alexk.patch | patch -p1 && \
	export LIBDIR=lib && \
	export INCDIR=include/qsbr && \
	export DESTDIR=/usr/local && \
	make all && \
	make install

RUN wget http://therouter.net/downloads/bpfjit.tar.gz && \
	wget http://therouter.net/downloads/sljit-0.92.tgz && \
	mkdir /usr/lib64 && \
	tar xzvf ./bpfjit.tar.gz && \
	tar xzvf ./sljit-0.92.tgz && \
	cd ./bpfjit/sljit/ && \
	cp -rpn ../../sljit-0.92/* ./ && \
	cd .. && \
	sed -e '/BuildRequires/s/^/#/g' -i ./SPECS/libbpfjit.spec && \
	make rpm && \
	rpm --nodeps -ihv RPMS/x86_64/libbpfjit-0.1-1.x86_64.rpm

RUN git clone -b alexk https://github.com/alexk99/npf && \
	wget http://therouter.net/downloads/city/city.h && \
	mv city.h /usr/local/include/ && \
	cd npf/src && \
	cd libnpf/net && \
	rm ./npf.h && \
	ln -s ../../kern/npf.h npf.h && \
	cd ../..  && \
	export DESTDIR=/ && \
	export LIBDIR=/usr/lib64 && \
	export INCDIR=/usr/local/include && \
	export MANDIR=/usr/local && \
	make && \
	make install

RUN wget http://therouter.net/downloads/quagga-1.0.20160315.tar.xz && \
	tar xvf ./quagga-1.0.20160315.tar.xz

RUN git clone https://github.com/google/cityhash && \
	cd ./cityhash/ && \
	./configure && \
	make all check CXXFLAGS="-g -O3" && \
	make install

RUN echo "/usr/lib64\n/usr/local/lib\n/usr/lib/x86_64-linux-gnu" > /etc/ld.so.conf.d/router.conf && \
	ldconfig

RUN wget https://fast.dpdk.org/rel/dpdk-17.11.1.tar.xz && \
	tar xvf dpdk-17.11.1.tar.xz && \
	cd ./dpdk-stable-17.11.1 && \
	wget http://therouter.net/downloads/dpdk/patches/17.11.1/eal_log.patch && \
	wget http://therouter.net/downloads/dpdk/patches/17.11.1/bond_fix_mtu.patch && \
	wget http://therouter.net/downloads/dpdk/patches/17.11.1/bond_lacp_fix_mempool_size.patch && \
	wget http://therouter.net/downloads/dpdk/patches/17.11.1/ip_fragmentation_table.patch && \
	wget http://therouter.net/downloads/dpdk/patches/17.11.1/bond_add_remove_mac_addr.patch && \
	cat ./eal_log.patch | patch -p1 && \
	cat ./bond_fix_mtu.patch | patch -p1 && \
	cat ./bond_lacp_fix_mempool_size.patch | patch -p1 && \
	cat ./ip_fragmentation_table.patch | patch -p1 && \
	cat ./bond_add_remove_mac_addr.patch | patch -p1 --ignore-whitespace && \
	sed -i s/CONFIG_RTE_LIBRTE_KNI=y/CONFIG_RTE_LIBRTE_KNI=n/ ./config/common_linuxapp && \
	sed -i s/CONFIG_RTE_KNI_KMOD=y/CONFIG_RTE_KNI_KMOD=n/ ./config/common_linuxapp && \
	sed -i s/CONFIG_RTE_LIBRTE_PMD_KNI=y/CONFIG_RTE_LIBRTE_PMD_KNI=n/ ./config/common_linuxapp && \
	sed -i s/CONFIG_RTE_EAL_IGB_UIO=y/CONFIG_RTE_EAL_IGB_UIO=n/ ./config/common_linuxapp && \
	make install T=x86_64-native-linuxapp-gcc

ENV THE_ROUTER_ARCHIVE='the_router.6cores.dpdk.17.11.1.pppoe.intel_nehalem.pppoe_a0.28.tar.gz'
ENV THE_ROUTER_DIRECTORY='the_router.latest.6cores.dpdk.17.11.1.pppoe.intel_nehalem'

RUN wget http://therouter.net/downloads/$THE_ROUTER_ARCHIVE && \
	tar xvf ./$THE_ROUTER_ARCHIVE && \
	cd ./$THE_ROUTER_DIRECTORY && \
	./install.sh

#CMD ["/usr/local/sbin/router_run.sh", "/etc/router.conf"]

ENV RTE_SDK='/dpdk-stable-17.11.1'

ENV COREMASK=0xF
ENV LCORES='0@0,1@1,2@2,3@3'
ENV NCHANNELS=2
ENV PCIWHITE='-w 0000:01:00.0 -w 0000:01:00.1'

CMD ["sh", "-c", "/usr/local/sbin/the_router", "--proc-type=primary", "-c $COREMASK", "--lcores=$LCORES", "--syslog='daemon'", "-n$NCHANNELS", "$PCIWHITE", "--", "-c /etc/router.conf", "-d"]