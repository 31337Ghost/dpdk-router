# Install

Here are the installations steps for Ubuntu 16.04.

## Install the following utilities and libs on host OS:

	apt-get update && \
        apt-get install -y --no-install-recommends \
        g++ \
        libjemalloc-dev \
        libpcap-dev \
        python \
        libpcre2-8-0 \
        autoconf \
        zlib1g-dev \
        flex \
        byacc \
        cmake \
        libtool \
        libtool-bin \
        libnuma-dev \
        unzip \
        wget

## Install DPDK

### Configure linux kernel

* Make sure the following options are enabled:

		UIO support
		   Device Drivers -> Userspace I/O drivers -> Generic driver for PCI 2.3 and PCI Express cards
		      symbol UIO_PCI_GENERIC
		
		PROC_PAGE_MONITOR
		   File systems -> Pseudo filesystems -> /proc file system support      
		
		HUGETLBFS
		  File systems -> Pseudo filesystems
		
		HPET and HPET_MMAP
		   Device Drivers -> Character devices -> HPET - High Precision Event Timer

* Turn on linux boot time options:
	- Edit GRUB_CMDLINE_LINUX variable in the /etc/default/grub
	
			GRUB_CMDLINE_LINUX="intel_idle.max_cstate=1 isolcpus=1,2,3,4,5,6,7,9,10,11,12,13,14,15 default_hugepagesz=2M hugepagesz=2M hugepages=3072"

	- Run
		
			update-grub

	- Note:
		You might want to isolate a different set of cores or reserve different amount of ram for huge pages 
		depending of the hardware configuration of your server.
		The rule here is that you should isolate all cores you're going to use in the router's traffic forwarding process unless
		the perfomance is not a goal.

* Configure hugepages

	- reboot you machine and check that hugepages are available and free

			grep -i huge /proc/meminfo

	- you should get something like this:

			HugePages_Total:    3072
			HugePages_Free:     3072
			HugePages_Rsvd:        0
			HugePages_Surp:        0
			Hugepagesize:       2048 kB

	- Make a mount point for hugepages

			mkdir /mnt/huge

	- Create a mount point entry in the /etc/fstab
	
			huge         /mnt/huge   hugetlbfs pagesize=2M   0       0
	
	- Mount hugepages
	
			mount huge
		
### Get DPDK, Patch and Compile

Run the following commands:		

    cd ~ && wget https://fast.dpdk.org/rel/dpdk-17.11.1.tar.xz && \
        tar xvf dpdk-17.11.1.tar.xz && \
        rm dpdk-17.11.1.tar.xz && \
        cd ./dpdk-stable-17.11.1 && \
        wget http://therouter.net/downloads/dpdk/patches/17.11.1/eal_log.patch && \
        wget http://therouter.net/downloads/dpdk/patches/17.11.1/bond_fix_mtu.patch && \
        wget http://therouter.net/downloads/dpdk/patches/17.11.1/bond_lacp_fix_mempool_size.patch && \
        wget http://therouter.net/downloads/dpdk/patches/17.11.1/ip_fragmentation_table.patch && \
        wget http://therouter.net/downloads/dpdk/patches/17.11.1/bond_add_remove_mac_addr.patch && \
        cat ./eal_log.patch | patch -p1 && \
        cat ./bond_fix_mtu.patch | patch -p1 && \
        cat ./bond_lacp_fix_mempool_size.patch | patch -p1 && \
        cat ./ip_fragmentation_table.patch | patch -p1 && \
        cat ./bond_add_remove_mac_addr.patch | patch -p1 --ignore-whitespace && \
        make install T=x86_64-native-linuxapp-gcc

### Configure dpdk

* Define $RTE_SDK variable

		export RTE_SDK=~/dpdk-stable-17.11.1

* Load drivers

		modprobe uio
		insmod $RTE_SDK/x86_64-native-linuxapp-gcc/kmod/igb_uio.ko
		# loading kni module 
		insmod $RTE_SDK/x86_64-native-linuxapp-gcc/kmod/rte_kni.ko

* Get your NIC's bus IDs

        # ethtool -i eth1 | grep bus-info
        bus-info: 0000:02:00.1

* Bind your NIC's to DPDK by using $RTE_SDK/usertools/dpdk-devbind.py

        $RTE_SDK/usertools/dpdk-devbind.py --bind=igb_uio 0000:02:00.1

* Also other driver could be used https://hub.docker.com/r/awsation/vpp-ubuntu-16.04/
        
        modprobe vfio-pci
        $RTE_SDK/usertools/dpdk-devbind.py --bind=vfio-pci 0000:02:00.1
        $RTE_SDK/usertools/dpdk-devbind.py --status
        echo 3072 > /proc/sys/vm/nr_hugepages

### Run TheRouter

 * Prepare configuration files. For configuration examples and options see the page <a href="/conf_options.md">Configure TheRouter</a>

 	- edit router.conf

			nano ./config/router.conf

 	- edit npf conf

			nano ./config/npf.conf

 * run the router

		docker-compose run -d
